# Purpose

This repository provides binary copies of my personal most recent installation of JModelica. Since the original source code is available from [JModelica.org](https://jmodelica.org), this repository serves as alternative to [install_jmodelica](https://gitlab.com/christiankral/install_jmodelica) and hopefully helps out some users in working with an "older" version JModelica under **Ubuntu 18.04**.

# Testing the Installation

Start `jmodelica.sh` from bash:

```python
from pyfmi import load_fmu
from pymodelica import compile_fmu
from pylab import *

fmu = compile_fmu('Modelica.Blocks.Examples.Filter',version = '2.0', target = 'me+cs', compiler = 'modelica', separate_process = False)
model = load_fmu(fmu)
opts = model.simulate_options()
opts["ncp"] = 10000
res = model.simulate(final_time=1,options=opts)
```
